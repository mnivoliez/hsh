#[macro_use]
extern crate clap;

use clap::{App, Arg, SubCommand};
use std::path::Path;

fn main() {
    let matches =
        App::new("hsh")
            .version(crate_version!())
            .about("hsh for Home Sweet Home, because you deserve it.")
            .subcommand(App::new("link").about("Ask hsh to link dotfiles into $HOMEDIR"))
            .arg(Arg::with_name("DOTFILES_DIR").help(
                "Path to the dotfiles directory. By default, $HOMEDIR/.dotfiles will be use.",
            ))
            .get_matches();

    match matches.subcommand_name() {
        Some("link") => println!("User ask to link its dotfiles"),
        _ => println!("No command called"),
    }
}
